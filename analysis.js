const axios = require('axios');
const fs = require('fs');
const csv = require('fast-csv');

// Define the API endpoint and API key
const API_URL = 'https://api.polygonscan.com/api?module=account&action=txlist&address=';
const API_KEY = 'M72KYXSB62ZTFGD58APVIJ76VEYQ9H97VY';

// Iterate through the transactions
let transactions = []

// Function to fetch transactions for a given address
async function getTransactions(address) {
  try {
    // Make the API call and parse the response 99999999
    const response = await axios.get(API_URL + address + '&startblock=0&endblock=99999999999&page=1&offset=10&sort=asc&apikey=' + API_KEY);
    const data = response.data;

    // Check if the API returned an error
    if (data.error) {
      console.log('Error: ' + data.error);
      return;
    }

    for (const transaction of data.result) {
      // Check the "to" and "from" fields to determine if the address has bought or sold a particular token
      // check also the value field of the transaction 
      if (transaction.to.toLowerCase() === address.toLowerCase()&&transaction.input !== "0x") {
        transactions.push({
            "txnType" : "Bought",
            "value": transaction.value,
            "fromAddress" : transaction.from,
            "timeStamp" : new Date(transaction.timeStamp * 1000)
        });
      } else if (transaction.from.toLowerCase() === address.toLowerCase()&&transaction.input !== "0x") {
        transactions.push({
            "txnType" : "Sold",
            "value": transaction.value,
            "toAddress" : transaction.to,
            "timeStamp" : new Date(transaction.timeStamp * 1000)
        });
      }
    }
    const ws = fs.createWriteStream('transactions.csv');
    console.log(transactions);
    csv.write(transactions, {headers: true}).pipe(ws);
    console.log('Transactions data saved to transactions.csv');
  } catch (error) {
    console.log('Error: ' + error);
  }
}
getTransactions('0x0204D69bd6cBDBf68E3d8f7AD30c5569DB763191');
